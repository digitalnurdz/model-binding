import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-custom-nested-form',
  templateUrl: './custom-nested-form.component.html',
  styleUrls: ['./custom-nested-form.component.scss']
})
export class CustomNestedFormComponent implements OnInit {

  @Input() model;

  constructor() { }

  ngOnInit() {
  }

}
