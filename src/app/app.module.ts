import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { CustomFormComponent } from './custom-form/custom-form.component';
import { CustomNestedFormComponent } from './custom-nested-form/custom-nested-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomFormComponent,
    CustomNestedFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
