import {Component, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Model Binding Example';

  @Output() myModel = {'first': 'Kenny', 'last': 'Yates',
  'street': '123 my street', 'city': 'Dallas'};
}
